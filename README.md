# rfmixer
===

RF signal mixer analyzer.

Enables quick analysis of spurs.

Receives input frequencies and parameters per frequency:
- beginning harmonic
- harmonic intervals
- duration

Each input frq is accompanied by its own instance of the interval.
Each interval is iterated separately.
Multiplies each interval to each frequency and adds/subtracts the products.

Outputs n number of n dimensional matrices (where n is the number of input frequencies).

Can be played back to the terminal, as a data file, or as a chart visualization in html via [Apache ECharts](https://echarts.apache.org/).

## Usage

```
RF signal mixer and spur analyzer.

Usage:
  rfmixer [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  mix         Mix signals with parameters
  serve       Serve a plot

Flags:
  -b, --begin float        offset relative to all signals
      --config string      config file
  -d, --duration int       number of samples (default (default 5)
  -f, --frq float64Slice   frequencies to mix (default [250.000000,250.000000])
  -h, --help               help for rfmixer
  -i, --interval int       distance between samples (default 5)
  -o, --output string      output file name (default "file.dat")
      --pretty             output pretty (default true)
      --visual string      visualizer method (default "chart")

Use "rfmixer [command] --help" for more information about a command.
```



## Example
```sh
[user@home /]$ rfmixer serve

[user@home /]$ rfmixer mix 17.7 9.8 --beginning 0 --interval 5 --duration 20
```
