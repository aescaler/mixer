package file

import (
	"rfmixer/pkg/compute"
	"rfmixer/pkg/globals"
	"strconv"

	"github.com/spf13/afero"
)

//
func Write(r []compute.Tensor) error {
	resultb := make([]byte, 0)

	resultb = append(resultb, []byte("# sum diff ")...)
	for _ = range globals.Frq {
		resultb = append(resultb, []byte("frq ")...)
	}

	resultb = append(resultb, []byte("beginning interval duration\n")...)

	// write header
	for _, fe := range globals.Frq {
		// write frequencies
		resultb = append(resultb, []byte(strconv.FormatFloat(fe, 'f', -1, 64)+" ")...)
	}
	// write parameters
	resultb = append(resultb, []byte(strconv.FormatFloat(globals.Beginning, 'f', -1, 64)+" "+strconv.Itoa(globals.Interval)+" "+strconv.Itoa(globals.Duration)+"\n\n")...)

	// write results
	for i := 0; i < len(r); i++ {
		// write data
		resultb = append(resultb, []byte(strconv.FormatFloat(r[i].Positive, 'f', -1, 64)+" ")...)
		resultb = append(resultb, []byte(strconv.FormatFloat(r[i].Negative, 'f', -1, 64)+" ")...)

		for e := range r[i].Factors {
			// this came true
			// one day I anticipate Factors will become a []float instead of []int
			resultb = append(resultb, []byte(strconv.FormatFloat(r[i].Factors[e], 'f', -1, 64)+" ")...)
		}

		resultb = append(resultb, []byte("\n")...)
	}

	// write file
	var appFS = afero.NewOsFs()

	return afero.WriteFile(appFS, globals.Output, resultb, 0600)
}
