package compute

import (
	"rfmixer/pkg/globals"
)

//
type Tensor struct {
	Positive float64
	Negative float64
	Factors  []float64
	Round    int
}

//
func recursor(q, m []int) ([]int, []int) {

	// while the last value of q is bigger than globals.Duration,
	// divide it by globals.Duration and do it again.
	for q[len(q)-1] >= (globals.Duration + 1) {
		q = append(q, q[len(q)-1]/(globals.Duration+1))
		m = append(m, m[len(m)-1]%(globals.Duration+1))
		return recursor(q, m)
	}
	return q, m
}

//
func padzeroes(m []int) []int {

	//
	if len(m) != len(globals.Frq) {
		g := make([]int, 0)
		g = append(g, 0)
		return padzeroes(append(g, m...))
	}
	return m
}

// TODO
// chunk res and use goroutines
func Result() ([]Tensor, error) {

	// quick exponent hack
	total := 1
	d := globals.Duration + 1
	for _ = range globals.Frq {
		total = total * (d)
	}

	// result size is (((globals.Duration+1)^len(globals.Frq))
	//									number of products
	res := make([]Tensor, total)

	for r := range res {

		// TODO
		// can probably use a logarithm to figure out exact size here using q and globals.Duration
		q, m := make([]int, 0), make([]int, 0)

		// convert r to base (globals.Duration)
		q = append(q, r/(globals.Duration+1))
		m = append(q, r%(globals.Duration+1))
		q, m = recursor(q, m)

		// relationship between len(m) and len(globals.Frq) is going to be important here
		// so we pad m with zeroes to make it the same size as globals.Frq
		m = padzeroes(m)

		// now m is the same size as globals.Frq. we can just multiply each term in m by globals.Duration and then add each of those to each term in globals.Frq.
		mf := make([]float64, len(m))
		for mi, mv := range m {
			mf[mi] = (float64((mv*globals.Interval)+1) * globals.Frq[mi]) + globals.Beginning
		}

		positive := 0.0
		negative := 0.0

		// TODO
		// this only supports two frequencies.
		// need to figure out how to do more.
		positive = mf[0] + mf[1]
		negative = mf[0] - mf[1]
		//	for z := range mf {
		//		positive = mf[z] + positive
		//		negative = mf[z] - negative
		//	}

		res[r].Positive = positive
		res[r].Negative = negative
		res[r].Factors = mf
		res[r].Round = r
	}
	return res, nil
}
