package graphics

import (
	"fmt"
	"io"
	"os"
	"rfmixer/pkg/compute"
	"rfmixer/pkg/globals"
	"strconv"

	"github.com/pterm/pterm"
)

//
func graphics2D(r []compute.Tensor) {

	switch globals.Visual {
	case "diagram":
		fmt.Println("Not implemented")
	// default is a table
	default:
		// write results
		ults := make([][]string, 0)
		res := make([]string, 0)
		for iw := 0; iw < len(r); iw++ {
			rese := fmt.Sprintf("%v,%v", strconv.FormatFloat(r[iw].Positive, 'f', -1, 64), strconv.FormatFloat(r[iw].Negative, 'f', -1, 64))
			res = append(res, rese)

			// i+1 to look ahead to the next object
			// % to tell where we are at in the object
			if ((iw + 1) % (globals.Duration + 1)) == 0 {
				ults = append(ults, res)
				res = make([]string, 0)
			}
		}

		if globals.PrettyPrint {
			pterm.DefaultTable.WithData(pterm.TableData(ults)).Render()
		} else {
			var out string
			for _, line := range ults {
				for _, lineItem := range line {
					out = fmt.Sprintf("%v\t%v", out, lineItem)
				}
				out = fmt.Sprintf("%v\n", out)
			}
			io.WriteString(os.Stdout, out)
		}
	}
}

//
func graphics3D(r []compute.Tensor) {

}

//
func selectAxes(r []compute.Tensor) {

}

func Show(t []compute.Tensor) error {

	// output result
	switch len(globals.Frq) {
	case 2:
		graphics2D(t)
	case 3:
		graphics3D(t)
	default:
		selectAxes(t)
	}

	return nil
}
