package graphics

import (
	"net/http"
	"rfmixer/pkg/compute"
	"rfmixer/pkg/globals"
	"strconv"

	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/opts"
	"github.com/go-echarts/go-echarts/v2/types"
)

//
func generateLineItems(r []compute.Tensor) []opts.LineData {

	//
	d := make([]opts.LineData, len(r))
	for i, v := range r {
		d[i].Value = v.Positive
	}

	return d
}

func httpLine(w http.ResponseWriter, r *http.Request) {

	// set up the chart
	line := charts.NewLine()
	line.SetGlobalOptions(
		charts.WithInitializationOpts(opts.Initialization{Theme: types.ThemeWesteros}),
		charts.WithTitleOpts(opts.Title{
			Title:    "Mixer Harmonics Chart",
			Subtitle: "",
		}))

	// TODO
	// this should go in the context too
	results, err := compute.Result()
	msg := "Could not compute result"
	if err != nil {
		http.Error(w, msg, http.StatusBadRequest)
		w.Write([]byte(msg))
	}

	//
	// axes := make([]opts.LineData, (len(results) / (globals.Duration + 1)))

	// map the data to the line chart
	// TODO
	// change globals to a struct with Duration as a field
	// add rest handling code and
	// - have initial function to parse values from body and store in context (similar to UserCredentials in TOOTES)
	for i := 0; i < len(results)/(globals.Duration+1); i++ {
		line.AddSeries(
			strconv.Itoa((i * globals.Interval)),
			generateLineItems(results[i:(i+(globals.Duration+1))]))
	}

	//
	xaxis := make([]string, globals.Duration+1)
	for j := range xaxis {
		xaxis[j] = strconv.FormatFloat(globals.Frq[0]+float64(j*globals.Interval), 'f', -1, 64)
	}

	//
	line.SetXAxis(xaxis)
	line.SetSeriesOptions(charts.WithLineChartOpts(opts.LineChart{Smooth: true}))

	//
	line.Validate()

	// write the chart to the http response
	line.Render(w)
}
