package graphics

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

//
func ServeHTTP() {

	//
	r := mux.NewRouter()
	r.HandleFunc("/graph/{type}", GraphHandler)
	http.Handle("/", r)

	//
	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:8000",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	//
	log.Fatal(srv.ListenAndServe())
}

//
func GraphHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	// OK listen.
	// I know.
	// this looks terrible.
	// it would look better done with a map[string]func(w http.ResponseWriter, r *http.Request, vars []string)
	// but, there is an upside.
	// the compiler optimizes switch for me.
	// so, to you, Mr. snarky future programmer, be warned.
	// I made this decision for a reason, and will defend it!
	switch vars["type"] {
	default:
		notImplemented(w, r, vars)
		//	case "bar":
		//		httpBarGraph(w, r, vars)
		//	case "bar3D":
		//		httpBar3D(w, r, vars)
		//	case "boxplot":
		//		httpBoxPlot(w, r, vars)
		//	case "cartesian3D":
		//		httpCartesian3D(w, r, vars)
		//	case "effectScatter":
		//		httpEffectScatter(w, r, vars)
		//	case "funnel":
		//		httpFunnel(w, r, vars)
		//	case "gauge":
		//		httpGauge(w, r, vars)
		//	case "geo":
		//		httpGeo(w, r, vars)
		//	case "graph":
		//		httpGraph(w, r, vars)
		//	case "heatmap":
		//		httpHeatMap(w, r, vars)
		//	case "candlestick":
		//		httpCandlestick(w, r, vars)
	case "line":
		httpLine(w, r)
		//	case "line3D":
		//		httpLine3D(w, r, vars)
		//	case "liquidFill":
		//		httpLiquidFill(w, r, vars)
		//	case "map":
		//		httpMap(w, r, args)
		//	case "parallel":
		//		httpParallel(w, r)
		//	case "pie":
		//		httpPie(w, r, args)
		//	case "radar":
		//		httpRadar(w, r, args)
		//	case "sankey":
		//		httpSankey(w, r, args)
		//	case "scatter":
		//		httpScatter(w, r, args)
		//	case "scatter3D":
		//		httpScatter3D(w, r, args)
		//	case "surface":
		//		httpSurface(w, r, args)
		//	case "themeRiver":
		//		httpThemeRiver(w, r, args)
		//	case "wordCloud":
		//		httpWordCloud(w, r, args)
		//	case "tree":
		//		httpTree(w, r, args)
		//	case "sunburst":
		//		httpSunburst(w, r, args)
	}
}

//
func notImplemented(w http.ResponseWriter, r *http.Request, v map[string]string) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, "404 not found type: %v\n", v["type"])
}
