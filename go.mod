module rfmixer

go 1.16

require (
	github.com/go-echarts/go-echarts/v2 v2.2.4
	github.com/gorilla/mux v1.8.0
	github.com/pterm/pterm v0.12.34
	github.com/spf13/afero v1.6.0
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
)
