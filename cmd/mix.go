/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"log"
	"rfmixer/pkg/compute"
	"rfmixer/pkg/file"
	"rfmixer/pkg/globals"
	"rfmixer/pkg/graphics"
	"strconv"

	"github.com/spf13/cobra"
)

// runs the default mixing profile
func rfmixer(args []string) {

	// set up globals.Frq
	f := make([]float64, len(args))
	for i, v := range args {
		s, err := strconv.ParseFloat(v, 64)
		if err != nil {
			log.Fatal(err)
		}
		f[i] = s
	}
	globals.Frq = f

	// compute result
	result, err := compute.Result()
	if err != nil {
		log.Fatalf(err.Error())
	}

	// show graphics
	if err := graphics.Show(result); err != nil {
		log.Fatalf(err.Error())
	}

	// write result
	if globals.Output != "" {
		if err := file.Write(result); err != nil {
			log.Fatalf(err.Error())
		}
	}
}

// mixCmd represents the mix command
var mixCmd = &cobra.Command{
	Use:   "mix FRQ1 FRQ2 [FRQN]... [flags]",
	Short: "Mix signals with parameters",
	Long: `Mix signals with parameters.
	Outputs either to a table in the console or as a data file.`,
	Run: func(cmd *cobra.Command, args []string) {
		rfmixer(args)
	},
	Args: cobra.MinimumNArgs(2),
}

func init() {
	rootCmd.AddCommand(mixCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// mixCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// mixCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
